var serverSocket = new SocketServer(22001, "string",
    function(out){
        print("서버 : 안녕하세요.");
        out.write("안녕하세요.");
    },
    function(msg, out){
        print("클라이언트1 : " + msg);
        print("서버 : 당신이 보낸 메세지는 \""+msg+"\" 입니다.");
        out.write("당신이 보낸 메세지는 \""+msg+"\" 입니다.");
    },
    function(error){
        print("서버 에러");
        print(error);
    });

serverSocket.open();

input();
serverSocket.close();