/**
 * Created by NHNEnt on 2017-02-24.
 */
var msg = "테스트";

function testCallBack(callBack){
    callBack();
}

testCallBack(function (){
    print(msg);
});

var thread1 = new Thread(function(){
    for(var i=0; i<3; i++){
        print("테스트 Thread1 " + i);
        sleep(1);
    }
});

var thread2 = new Thread(function(){
    for(var i=0; i<3; i++){
        print("테스트 Thread2 " + i);
        sleep(1);
    }
});

var thread3 = new Thread(function(){
    for(var i=0; i<3; i++){
        print("테스트 Thread3 " + i);
        sleep(1);
    }
});

print("스레드테스트입니다1 ");
thread1.start();
thread2.start();
thread3.start();
print("스레드테스트입니다2 ");

while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive()){
    sleep(1);
}

print("아무키나 입력하고 엔터를 누르세요.");
print(input());

include("./test2.js");