package test;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Test1 {
    public static void main(String[] args){
        int[] aa = IntStream.iterate(1, n -> n+1).limit(988).toArray();
        List<Integer> list1 = Arrays.stream(aa).boxed().collect(Collectors.toList());
        List<List<Integer>> lists = Lists.partition(list1, 200);

        System.out.println(lists);
    }
}
