package paser;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.*;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class Paser {

    public static void main(String[] args) throws IOException {
        File input = new File("D:\\c.log");
        File output = new File("D:\\c.out.log");
        final int[] cnt = {0, 0};
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(output))){
            StringBuilder stringBuilder = new StringBuilder();
            try(BufferedReader fileReader = new BufferedReader(new FileReader(input))){
                String s;

                HashMap<String, PushNoticeModel> map = new LinkedHashMap<>();

                while((s = fileReader.readLine()) != null){
                    PushNoticeModel pushNoticeModel = parsePushNoticeModel(s);
                    map.put(pushNoticeModel.getOrderNo(), pushNoticeModel);
                }

                System.out.println("총 : " + map.size());

                map.forEach((key, pushNoticeModel) ->{
                    String query = "INSERT INTO push_notice(app_service_id, payco_id_no, display_ymdt, noti_type_code, message_text, req_noti_key, req_svc_id, req_unique_key,\n" +
                            "                mod_ymdt, reg_ymdt, categorize_code, notice_message_text, sender_type_code, sender_key, sender_name, sender_bi_url)\n" +
                            "        VALUES('"+pushNoticeModel.getAppServiceId()+"', '"+pushNoticeModel.getIdNo()+"', '"+pushNoticeModel.getDisplayDateStr()+"', '"+pushNoticeModel.getType()+"', COMPRESS('"+pushNoticeModel.getMessage()+"'), '"+pushNoticeModel.getReqNoticeKey()+"', '"+pushNoticeModel.getReqServiceId()+"', "+ ((pushNoticeModel.getReqUniqueKey() == null) ? "null":"'"+pushNoticeModel.getReqUniqueKey()+"'") + ",\n" +
                            "        NOW(), NOW(), '"+pushNoticeModel.getCategorizeCode()+"', COMPRESS('"+pushNoticeModel.getNoticeMessage()+"'), '"+pushNoticeModel.getSenderType()+"', '"+pushNoticeModel.getSenderKey()+"', '"+pushNoticeModel.getSenderName()+"', '"+pushNoticeModel.getSenderKey()+"')";

                    System.out.println(query);
                    System.out.println();
                });
            }
        }

//        INSERT INTO push_notice(app_service_id, payco_id_no, display_ymdt, noti_type_code, message_text, req_noti_key, req_svc_id, req_unique_key,
//                mod_ymdt, reg_ymdt, categorize_code, notice_message_text, sender_type_code, sender_key, sender_name, sender_bi_url)
//        VALUES(#{appServiceId}, #{paycoIdNo}, #{displayDate}, #{type}, COMPRESS(#{message}), #{reqNoticeKey}, #{reqServiceId}, #{reqUniqueKey},
//        NOW(), NOW(), #{categorizeCode}, COMPRESS(#{noticeMessage}), #{senderType}, #{senderKey}, #{senderName}, #{senderBiUrl})
    }

    private static PushNoticeModel parsePushNoticeModel(String msg) throws UnsupportedEncodingException {

        msg = msg.replace(">>\t", "");

        JSONObject jsonObject = new JSONObject();
        String[] array = msg.split("&");
        for(String keyValue : array){
            String[] kv = keyValue.split("=");

            jsonObject.put(URLDecoder.decode(kv[0], "utf-8"), URLDecoder.decode(kv[1], "utf-8"));
        }

        return JacksonUtil.toObject(jsonObject.toJSONString(), PushNoticeModel.class);
    }

    @Data
    static class PushNoticeModel {
        private String appServiceId;    // npush에서 제공한 앱의 서비스 아이디
        private String idNo;
        private Date displayDate;   // YYYY-MM-DDTHH:MM:SS.SSSZ
        // SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        private String type;    // ex)주문안내/결제안내/포인트안내
        private String message;
        private String reqNoticeKey;    // 요청한 곳의 notice key
        private String reqServiceId;    // 요청한 곳의 서비스 아이디
        private String reqUniqueKey;	//요청한 곳에서의 UniqueKey(중복발송 방지를 위해 추가)
        private String readYN;
        private String categorizeCode;
        private String osType;
        private String displayYn;

        private String noticeMessage;
        private String senderType;
        private String senderKey;
        private String senderName;
        private String senderBiUrl;

        private String getOrderNo(){
            return (String) JacksonUtil.toMap(message).get("orderNo");
        }

        @JsonIgnore
        private int offset;
        @JsonIgnore private int limit;

        public String getDisplayDateStr() {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            return sdf.format(this.getDisplayDate());
        }
    }

//    public static void main(String[] args){
//
//        final Thread client = new Thread(() -> {
//            OkHttpClient okHttpClient = new OkHttpClient();
//            Retrofit retrofit = new Retrofit.Builder().baseUrl("http://localhost:22001").client(okHttpClient).build();
//
//            HashMap<String, RequestBody> param = new HashMap<>();
//            param.put("questionContent", RequestBody.create(null, "테스트입니다."));
//            param.put("deviceInfo", RequestBody.create(null, "Galaxy S8+"));
//            param.put("osInfo", RequestBody.create(null, "Android 7.0"));
//            param.put("appInfo", RequestBody.create(null, "1233"));
//            param.put("tagSeq", RequestBody.create(null, "1"));
//
//            MultipartBody.Part[] imsgeFiles = new MultipartBody.Part[]{
//                    MultipartBody.Part.createFormData("imageFiles", "image1.png", RequestBody.create(MediaType.parse("image/png"), new File("D:\\업무자료\\testImage.png"))),
//                    MultipartBody.Part.createFormData("imageFiles", "image1.png", RequestBody.create(MediaType.parse("image/png"), new File("D:\\업무자료\\testImage2.png"))),
//            };
//            ApiSubmit apiSubmit = retrofit.create(ApiSubmit.class);
//            apiSubmit.submit("AAAA/MMNeEH60G9IwbXH4LSvvi6qQ1X0e9hjx7wbvXyHQoHK1En8V1sqbpXvhyLjxGjEdzvFuhCvg7c4HwYr9v8Xtb56JqQ7EIa+ymiIURhk8M23LxidB9ebJtSfhKahgwQ7Ws4vmuvf9lXUFeEdKwJ6PJPs9FLrhKTwAOkAaAJz92A8BgsZybIQwLkUtH7RmlsCKin3oRjdP3DZIoAy+YY1ciNAroM+JhEh0azLFZjdoAmR0jbNU7BD0cmrQZPu+Lkgc9jmNA6ObSvAjqa+tJR/XdtESKpz534iIHW3dxQzTcPlJ063qFpC+Mdx7lMjavo8xXXcX1GhZfLd3ph6z8YMrrU=",
//                    "ALPVNKH9DpKXNr0ILiFH",
//                    "",
//                    "",
//                    param,
//                    imsgeFiles);
//        });
//
//        final Thread server = new Thread(() -> {
//            try {
//                ServerSocket serverSocket = new ServerSocket(22001);
//                Socket socket = serverSocket.accept();
//
//                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
//                while(socket.isConnected()){
//                    String line = bufferedReader.readLine();
//                    if(line == null) {
//                        try {
//                            Thread.sleep(100);
//                        } catch (InterruptedException e) {
//                        }
//                        continue;
//                    }
//
//                    System.out.println(line);
//                }
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        });
//        server.start();
//
//        client.start();
//    }
}

//interface ApiSubmit {
//    @Multipart
//    @POST
//    Call<String> submit(@Header("access_token") String access_token,
//                        @Header("client_id") String client_id,
//                        @Header("user-agent") String user_agent,
//                        @Header("connection") String connection,
//                        @PartMap Map<String, RequestBody> param1,
//                        @Part("imageFiles") MultipartBody.Part[] files);
//}