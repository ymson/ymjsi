package com.ymson.jsi;

import com.ymson.jsi.interpreter.JSInterPreter;
import com.ymson.jsi.interpreter.reporter.YMErrorReporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;

public class Main {
    static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws InvocationTargetException, IOException, InstantiationException, IllegalAccessException {
//        new JSInterPreter(true, new File("test.js"));
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try(JSInterPreter interpreter = new JSInterPreter(YMErrorReporter.getInstance())) {

            System.out.print("cmd:> ");
            while(true){
                boolean isBackGround = false;
                boolean debug = false;
                String command = br.readLine();
                String[] concatCmd = command.split(" ");
                int cmdPointer = 0;
                switch(concatCmd[cmdPointer++]){
                    case "debug":
                        debug = true;
                    case "start":
                        if(!debug) isBackGround = true;
                    case "run":
                        final boolean doDebug = debug;
                        File[] jsFile = new File[concatCmd.length - 1];
                        for(int i=0; cmdPointer<concatCmd.length; i++, cmdPointer++){
                            jsFile[i] = new File(concatCmd[cmdPointer]);
                        }

                        if(isBackGround){
                            new Thread(() -> {
                                try {
                                    interpreter.processScript(doDebug, jsFile);
                                } catch (Exception e) {
                                    LOGGER.error("", e);
                                }
                            }).start();
                        }else{
                            try {
                                interpreter.processScript(doDebug, jsFile);
                            } catch (Exception e) {
                                LOGGER.error("", e);
                            }
                        }

                        break;
                    case "":
                        break;
                    case "exit":
                        System.exit(0);
                        break;
                    default:
                        System.out.println("Can't find command " + concatCmd[0]);
                        break;

                }
                System.out.print("cmd:> ");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
