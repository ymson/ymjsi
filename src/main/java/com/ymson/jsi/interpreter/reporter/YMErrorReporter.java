package com.ymson.jsi.interpreter.reporter;

import org.mozilla.javascript.AssertException;
import org.mozilla.javascript.ErrorReporter;
import org.mozilla.javascript.EvaluatorException;

/**
 * Created by NHNEnt on 2017-02-28.
 */
public class YMErrorReporter implements ErrorReporter{

    static ErrorReporter me;

    public static ErrorReporter getInstance(){
        if(me == null) me = new YMErrorReporter();

        return me;
    }

    private String formatError(String message, String sourceName, int line, int lineOffset) {
        return message + " at (" + sourceName + "#[" + line + "," + lineOffset + "])";
    }

    @Override
    public void warning(String message, String sourceName, int line, String lineSource, int lineOffset) {
        String str = "[WARNING] " + this.formatError(message, sourceName, line, lineOffset);
        System.out.println(str);
    }

    @Override
    public void error(String message, String sourceName, int line, String lineSource, int lineOffset) {
        throw new EvaluatorException("[ERROR] " + message + " at", sourceName, line, lineSource, lineOffset);
    }

    @Override
    public EvaluatorException runtimeError(String message, String sourceName, int line, String lineSource, int lineOffset) {
        throw new EvaluatorException(message + " at", sourceName, line, lineSource, lineOffset);
    }

    @Override
    public AssertException assertError(boolean state, String message, String sourceName, int line, String lineSource, int lineOffset) {
        if(state) return new AssertException(state, message + " at", sourceName, line, lineSource, lineOffset);
        else throw new AssertException(state, message + " at", sourceName, line, lineSource, lineOffset);
    }
}
