package com.ymson.jsi.interpreter;

import com.ymson.jsi.shellscript.console.Console;
import com.ymson.jsi.shellscript.socket.Socket;
import com.ymson.jsi.shellscript.socket.SocketServer;
import com.ymson.jsi.shellscript.thread.JSThread;
import org.mozilla.javascript.*;
import org.mozilla.javascript.tools.debugger.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by NHNEnt on 2017-02-24.
 */
public class JSInterPreter implements  Closeable {
    private static final Logger LOGGER = LoggerFactory.getLogger(JSInterPreter.class);

    Scriptable currentScope;
    Scriptable top;
    ErrorReporter reporter;
    ContextFactory factory;
    Context context;
    Main debugger = null;
    public JSInterPreter(ErrorReporter reporter) {
        this.reporter = reporter;
    }

    public boolean processScript(boolean debug, File... jsFiles) throws IllegalAccessException, InvocationTargetException, InstantiationException, IOException{

        for(File jsFile : jsFiles){

            LOGGER.info("Run YMJsi : " + jsFile.getAbsolutePath());

            this.factory = new ContextFactory();

            debugger = null;

            if(debug){
                debugger = Main.mainEmbedded(null, "YMJsi Debugger - " + Thread.currentThread().getName(), jsFile.getParentFile());
                debugger.attachTo(factory);
            }

            this.context = this.factory.enterContext();
            assert this.context != null;
            this.top = this.context.initStandardObjects();
            this.context.setErrorReporter(reporter);
//            this.context.setOptimizationLevel(-1);


            LOGGER.info("******** Load default resources ********");
            LOGGER.info("[Default ShellScript]");
            List<String> sList = appendShellScript(Console.class, JSThread.class, SocketServer.class, Socket.class);
            for(String sName : sList) {
                LOGGER.info("\t" + sName);
            }
            LOGGER.info("****************************************");

            try(FileReader reader = new FileReader(jsFile)) {
                currentScope = context.newObject(top, "Console");
                if(currentScope instanceof Console){
                    ((Console)currentScope).setCurrentScriptFile(jsFile);
                }
                if(debugger != null){

                    debugger.setBreakOnEnter(false);
                    debugger.setBreakOnExceptions(false);
                    debugger.setBreakOnReturn(false);
                    debugger.setScope(currentScope);
                    debugger.setSize(640, 400);
                    debugger.setAlwaysOnTop(true);
                    debugger.setVisible(true);
                    debugger.doBreak();
                    debugger.setExitAction(new Runnable(){

                        @Override
                        public void run() {
                            debugger.clearAllBreakpoints();
                            context.terminate();
                        }
                    });
                }
                context.evaluateReader(currentScope, reader, jsFile.getAbsolutePath(), 1,  null);
            }catch (Exception e){
                LOGGER.info("", e);
            }finally {
                Context.exit();
            }
        }

        return true;
    }

    @Override
    public void close() throws IOException {
    }

    public List<String> appendShellScript(Class<? extends ScriptableObject>... shellScriptClasses){

        ArrayList<String> sList = new ArrayList<>();

        for(Class<? extends ScriptableObject> shellScriptClass : shellScriptClasses){

            try {
                ScriptableObject sobj = shellScriptClass.newInstance();
                String moduleName = sobj.getClassName();
                ScriptableObject.defineClass(top, shellScriptClass);
                sList.add(moduleName);
            } catch (Exception e) {
                LOGGER.info("", e);
            }
        }

        return sList;
    }
}
