package com.ymson.jsi.shellscript;

import java.io.File;

/**
 * Created by NHNEnt on 2017-03-02.
 */
public interface ScriptStatus {
    void setCurrentScriptFile(File file);
    File getCurrentScriptFile();
    File getCurrentDir();
}
