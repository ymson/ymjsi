package com.ymson.jsi.shellscript.console;

import com.ymson.jsi.shellscript.ScriptStatus;
import com.ymson.jsi.shellscript.ShellScript;
import org.mozilla.javascript.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

@SuppressWarnings("ALL")
public class Console extends ShellScript implements ScriptStatus {
    private static final Logger LOGGER = LoggerFactory.getLogger(Console.class);
    File currentScriptFile;

    public void jsFunction_sleep(int millis) throws InterruptedException {
        Thread.sleep(millis);
    }

    public void jsFunction_print(String msg){
        System.out.println(msg);
    }

    public String jsFunction_input(String msg){

        if(msg == null || UNDEFINED.equals(msg)) msg = "";

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print(msg);
        try {
            return br.readLine();
        } catch (IOException e) {
            return "";
        }
    }

    public void jsFunction_include(String jsFile) throws IOException {
        Context cx = Context.getCurrentContext();
        String inputFile = jsFile.replace("/", "\\");
        File f;
        if(inputFile.indexOf(":\\") != -1){
            f = new File(inputFile);
        }else if(inputFile.startsWith(".\\") || inputFile.startsWith("..\\")){
            f = new File(getCurrentDir(), inputFile);
        }else{
            f = new File(System.getProperty("user.dir"), inputFile);
        }

        if(!f.exists()){
            Context.reportRuntimeError("ReferenceError: " + f.getAbsolutePath().toString() + " does not exist.");
            return;
        }

        File prevScript = getCurrentScriptFile();

        try(Reader reader = new FileReader(f)){
            setCurrentScriptFile(f);
            LOGGER.info("Include : " + f.getAbsolutePath());
            cx.evaluateReader(this, reader, f.getAbsolutePath(), 1, null);
        }finally {
            setCurrentScriptFile(prevScript);
        }
    }

    @Override
    public void setCurrentScriptFile(File file) {
        currentScriptFile = file;
    }

    @Override
    public File getCurrentScriptFile() {
        return currentScriptFile;
    }

    @Override
    public File getCurrentDir() {
        return getCurrentScriptFile().getParentFile();
    }

    @Override
    public void distory() throws Throwable {

    }
}
