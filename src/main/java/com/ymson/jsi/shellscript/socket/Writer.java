package com.ymson.jsi.shellscript.socket;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;

public class Writer {
    ChannelHandlerContext ctx;


    public Writer(ChannelHandlerContext ctx){
        this.ctx = ctx;
    }

    public void write(String msg){
        write(msg.getBytes());
    }

    public void write(byte[] data){
        ByteBuf message = Unpooled.buffer(data.length);
        message.writeBytes(data);
        this.ctx.writeAndFlush(message);
    }
}
