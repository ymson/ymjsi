package com.ymson.jsi.shellscript.socket;

import com.ymson.jsi.shellscript.ShellScript;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.NativeArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("ALL")
public class Socket extends ShellScript implements Constants {
    private static final Logger LOGGER = LoggerFactory.getLogger(Socket.class);

    String url;
    int port;
    Context jsCtx;
    EventLoopGroup group;
    Bootstrap b;
    ChannelFuture cf;
    ChannelHandlerContext currentChannelContext;
    public void jsConstructor(String url, int port, String dataType, Function active, Function read, Function error) {

        jsCtx = Context.getCurrentContext();
        this.url = url;
        this.port = port;
        group = new NioEventLoopGroup();

        b = new Bootstrap();
        b.group(group)
                .channel(NioSocketChannel.class)
                .option(ChannelOption.TCP_NODELAY, true)
                .handler(new LoggingHandler(LogLevel.DEBUG))
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel sc) throws Exception {
                        ChannelPipeline cp = sc.pipeline();
                        cp.addLast(new ChannelHandlerAdapter(){
                            @Override
                            public void channelActive(ChannelHandlerContext ctx) throws Exception {
                                currentChannelContext = ctx;
                                if(active != null) active.call(jsCtx, getParentScope(), Socket.this, new Object[]{new Writer(ctx)});
                            }

                            @Override
                            public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                                ByteBuf byteBufMessage = (ByteBuf) msg;
                                int size = byteBufMessage.readableBytes();
                                byte [] byteMessage = new byte[size];
                                msg = byteMessage;
                                if(DATA_TYPE_STRING.equals(dataType.toLowerCase())){
                                    byteBufMessage.getBytes(0, byteMessage);
                                    String str = new String(byteMessage);
                                    msg = str;
                                }
                                if(read != null) read.call(jsCtx, getParentScope(), Socket.this, new Object[]{msg, new Writer(ctx)});
                            }

                            @Override
                            public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
                                ctx.flush();
                            }

                            @Override
                            public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
                                    throws Exception {
                                ctx.close();
                                if(error != null) error.call(jsCtx, getParentScope(), Socket.this, new Object[]{cause});
                            }
                        });
                    }
                });
    }

    public void jsFunction_open() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    cf = b.connect(url, port).sync();
                    cf.channel().closeFuture().sync();
                } catch (InterruptedException e) {
                    LOGGER.error("", e);
                } finally {
                    distory();
                }
            }
        }).start();
    }

    public void jsFunction_send(String msg){
        if(currentChannelContext == null){
            Context.reportError("Socket is not active yet");
            return;
        }

        new Writer(currentChannelContext).write(msg);
    }

    public void jsFunction_sendBytes(NativeArray msg){
        byte[] data = new byte[(int)msg.getLength()];
        Object[] ids = msg.getIds();
        for(Object id : ids){
            int index = (Integer) id;
            assert msg.get(index, null) instanceof Number;
            byte b = (byte)msg.get(index, null);
            data[index] = b;
        }
        if(currentChannelContext == null){
            Context.reportError("Socket is not active yet");
            return;
        }

        new Writer(currentChannelContext).write(data);
    }


    public void jsFunction_close() {
        distory();
    }

    @Override
    public void distory(){
        if(group != null) {
            group.shutdownGracefully();
            group = null;
        }
    }
}
