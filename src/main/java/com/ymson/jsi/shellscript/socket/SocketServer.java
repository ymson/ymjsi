package com.ymson.jsi.shellscript.socket;

import com.ymson.jsi.shellscript.ShellScript;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.NativeArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("ALL")
public class SocketServer extends ShellScript implements Constants {

    private static final Logger LOGGER = LoggerFactory.getLogger(SocketServer.class);

    int port;
    Context jsCtx;
    EventLoopGroup bossGroup;
    EventLoopGroup workerGroup;
    ServerBootstrap sb;
    ChannelFuture future;
    ChannelHandlerContext currentChannelContext;
    public void jsConstructor(int port, String dataType, Function active, Function read, Function error) {
        this.port = port;
        jsCtx = Context.getCurrentContext();

        bossGroup = new NioEventLoopGroup(1);
        workerGroup = new NioEventLoopGroup();
        sb = new ServerBootstrap();
        sb.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .option(ChannelOption.SO_REUSEADDR, true)
                .option(ChannelOption.SO_BACKLOG, 100)
                .handler(new LoggingHandler(LogLevel.INFO))
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    public void initChannel(SocketChannel socketChannel) throws Exception {
                        ChannelPipeline cp = socketChannel.pipeline();
                        cp.addLast(new ChannelHandlerAdapter() {
                            @Override
                            public void channelActive(ChannelHandlerContext ctx) throws Exception {
                                currentChannelContext = ctx;
                                if(active != null) active.call(jsCtx, getParentScope(), SocketServer.this, new Object[]{new Writer(ctx)});
                            }

                            @Override
                            public void channelRead(ChannelHandlerContext ctx, Object msg)
                                    throws Exception {
                                ByteBuf byteBufMessage = (ByteBuf) msg;
                                int size = byteBufMessage.readableBytes();
                                byte [] byteMessage = new byte[size];
                                msg = byteMessage;
                                if(DATA_TYPE_STRING.equals(dataType.toLowerCase())){
                                    byteBufMessage.getBytes(0, byteMessage);
                                    String str = new String(byteMessage);
                                    msg = str;
                                }

                                if(read != null) read.call(jsCtx, getParentScope(), SocketServer.this, new Object[]{msg, new Writer(ctx)});
                            }

                            @Override
                            public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
                                ctx.flush();
                            }

                            @Override
                            public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
                                    throws Exception {
                                ctx.close();
                                if(error != null) error.call(jsCtx, getParentScope(), SocketServer.this, new Object[]{cause});
                            }
                        });
                    }
                });
    }

    public void jsFunction_open() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    future = sb.bind(port).sync();
                    future.channel().closeFuture().sync();
                } catch (InterruptedException e) {
                    LOGGER.error("", e);
                } finally {
                    distory();
                }
            }
        }).start();

    }

    public void jsFunction_send(String msg){
        if(currentChannelContext == null){
            Context.reportError("Socket is not active yet");
            return;
        }

        new Writer(currentChannelContext).write(msg);
    }

    public void jsFunction_sendBytes(NativeArray msg){
        byte[] data = new byte[(int)msg.getLength()];
        Object[] ids = msg.getIds();
        for(Object id : ids){
            int index = (Integer) id;
            assert msg.get(index, null) instanceof Number;
            byte b = (byte)msg.get(index, null);
            data[index] = b;
        }
        if(currentChannelContext == null){
            Context.reportError("Socket is not active yet");
            return;
        }

        new Writer(currentChannelContext).write(data);
    }

    public void jsFunction_close() {
        distory();
    }

    @Override
    public void distory() {
        if(workerGroup != null) {
            workerGroup.shutdownGracefully();
            workerGroup = null;
        }
        if(bossGroup != null) {
            bossGroup.shutdownGracefully();
            bossGroup = null;
        }
    }
}
