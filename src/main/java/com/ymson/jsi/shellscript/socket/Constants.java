package com.ymson.jsi.shellscript.socket;

/**
 * Created by NHNEnt on 2017-03-02.
 */
public interface Constants {
    public static final String DATA_TYPE_BYTE = "byte";
    public static final String DATA_TYPE_STRING = "string";
}
