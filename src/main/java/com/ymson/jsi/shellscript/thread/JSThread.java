package com.ymson.jsi.shellscript.thread;

import com.ymson.jsi.shellscript.ShellScript;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.EcmaError;
import org.mozilla.javascript.Function;
@SuppressWarnings("ALL")
public class JSThread extends ShellScript {

    Thread myThread;
    Function jsFunction;
    Context ctx;

    @Override
    public String getClassName() {
        return "Thread";
    }

    public void jsConstructor(Function func){
        ctx = Context.getCurrentContext();
        jsFunction = func;
    }

    public void jsFunction_start(){
        if(jsFunction == null) return;

        myThread = new Thread(() -> {
            while(true){
                try{
                    jsFunction.call(ctx, JSThread.this, JSThread.this, null);
                    return;
                }catch (IllegalStateException | EcmaError e){
                    try{
                        Thread.sleep(1);
                    }catch (Exception ignore){}
                    continue;
                }
            }

        });

        myThread.start();
    }

    public boolean jsFunction_isAlive(){
        return (myThread == null) ? false : myThread.isAlive();
    }

    @Override
    public void distory() throws Throwable {

    }
}
