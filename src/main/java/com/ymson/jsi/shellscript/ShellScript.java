package com.ymson.jsi.shellscript;

import org.mozilla.javascript.ScriptableObject;

public abstract class ShellScript extends ScriptableObject {

    public static final String UNDEFINED = "undefined";

    @Override
    public String getClassName() {
        return this.getClass().getSimpleName();
    }
}
