package org.mozilla.javascript.tools.debugger;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JInternalFrame;

public class PlatFormInternalFrame extends JInternalFrame implements FocusListener {
	
	public static final Rectangle DEFAULT_Rectangle = new Rectangle(0,0,200, 200);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create the frame.
	 */
	
	public PlatFormInternalFrame(String title, boolean hide_on_close) {
		this(title);
		setBounds(DEFAULT_Rectangle);
		if(hide_on_close) setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE);
		else setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);

		setFocusable(true);
		setRequestFocusEnabled(true);
	}
	
	public PlatFormInternalFrame(String title, int x, int y, int width, int height, boolean hide_on_close) {
		this(title);
		setBounds(new Rectangle(x, y, width, height));
		if(hide_on_close) setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE);
		else setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
	}
	
	public void setHideOnClose(boolean b){
		if(b) setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE);
		else setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
	}
	
	private PlatFormInternalFrame(String title) {
		//specified title, resizability, closability, maximizability, and iconifiability. 
		super(title, true, true, true, true);
		this.getContentPane().addFocusListener(this);
	}
	
	@Override
	public void setVisible(boolean isVisible){
		if(isVisible && this.getParent() == null){
			try {
				throw new Exception("This Frame has not been adapted in PlatForm.");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		}
		super.setVisible(isVisible);
	}
	
	public void close(){
		setVisible(false);
//		super.dispose();
		doDefaultCloseAction();
	}

	@Override
	public final void focusGained(FocusEvent e){
	}

	@Override
	public final void focusLost(FocusEvent e){

	}

}
