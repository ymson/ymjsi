package org.mozilla.javascript.tools.debugger;


import java.awt.Component;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

abstract public class IMAGEICON extends ImageIcon {
	/**
	 * 
	 */
	
	int width = 0;
	int height = 0;
	
	public void setSize(int w, int h){
		width = w;
		height = h;
	}
	
	private static final long serialVersionUID = 1L;
	
	public IMAGEICON(Class<?> c, String imgIcon){
		super(c.getResource(imgIcon));
		width = getImage().getWidth(getImageObserver());
		height = getImage().getHeight(getImageObserver());
	}
	
	public IMAGEICON(Class<?> c, String path, String filename){
		this(c, path + "/" + filename);
	}
	
	public static BufferedImage getImage(Class<?> c, String filename) throws IOException{
//		return (new IMAGEICON(path)).getImage();
		return ImageIO.read(c.getResource(filename));
	}
	
	public static BufferedImage getImage(Class<?> c, String path, String filename) throws IOException{
		return getImage(c, path + "/" + filename);
	}
	
    public synchronized void paintIcon(Component c, Graphics g, int x, int y) {
		if(getImageObserver() == null) {
			g.drawImage(getImage(), x, y, width, height, c);
		} else {
			g.drawImage(getImage(), x, y, width, height, getImageObserver());
		}
    }
    
    public int getIconWidth() {
    	return width;
    }

    public int getIconHeight() {
    	return height;
    }
}
