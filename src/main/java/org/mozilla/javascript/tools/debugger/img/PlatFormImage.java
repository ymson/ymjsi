package org.mozilla.javascript.tools.debugger.img;

import java.awt.image.BufferedImage;
import java.io.IOException;

import org.mozilla.javascript.tools.debugger.IMAGEICON;

public class PlatFormImage extends IMAGEICON {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String defaultpath= "icon";
	public static final String fileName_Activation = "Activation.png";
	public static final String fileName_add = "add.gif";
	public static final String fileName_application = "application.gif";
	public static final String fileName_auth = "auth.gif";
	public static final String fileName_bullet = "bullet.gif";
	public static final String fileName_clear = "clear.gif";
	public static final String fileName_close = "close.gif";
	public static final String fileName_D = "D-icon.png";
	public static final String fileName_E = "E-icon.png";
	public static final String fileName_U = "U-icon.png";
	public static final String fileName_V = "V-icon.png";
	public static final String fileName_down = "down.png";
	public static final String fileName_ClosedFolder = "ClosedFolder.png";
	public static final String fileName_collapse = "collapse.png";
	public static final String fileName_expended = "expended.png";
	public static final String fileName_File = "File.png";
	public static final String fileName_fileContents = "fileContents.gif";
	public static final String fileName_keyManager = "key_manager.gif";
	public static final String fileName_off = "off.gif";
	public static final String fileName_on = "on.gif";
	public static final String fileName_OpenedFolder = "OpenedFolder.png";
	public static final String fileName_package = "package.png";
	public static final String fileName_package_obj = "package_obj.gif";
	public static final String fileName_readerOn_24 = "readerOn_24.png";
	public static final String fileName_readerOff_24 = "readerOff_24.png";
	public static final String fileName_stop = "stop.gif";
	public static final String fileName_run_script = "run_script.gif";
	public static final String fileName_save_all = "save_all.png";
	public static final String fileName_save = "save.jpg";
	public static final String fileName_search_next = "search_next.gif";
	public static final String fileName_splash = "splash.gif";
	public static final String fileName_titleIcon1 = "titleIcon1.png";
	public static final String fileName_titleIcon2 = "titleIcon2.png";
	public static final String fileName_up = "up.png";
	public static final String fileName_upload_cap = "upload_cap.gif";
	public static final String fileName_windowIcon = "windowIcon.gif";
	public static final String fileName_checkedIcon = "checked.png";
	public static final String fileName_uncheckedIcon = "unchecked.png";
	public static final String fileName_disabledcheckedIcon = "disabledchecked.png";
	public static final String fileName_disableduncheckedIcon = "disabledunchecked.png";
	public static final String fileName_selectedRadioIcon = "selectedradio.png";
	public static final String fileName_unselectedRadioIcon = "unselectedradio.png";
	public static final String fileName_disabledSelectedRadioIcon = "disabledselectedradio.png";
	public static final String fileName_disabledUnselectedRadioIcon = "disabledunselectedradio.png";
	public static final String fileName_dot = "dot.png";
	public static final String fileName_message_warning = "message_warning.gif";
	
	public PlatFormImage(String fileName) {
		super(PlatFormImage.class, defaultpath + "/" + fileName);
	}
	
	public PlatFormImage(String path, String fileName) {
		super(PlatFormImage.class, path + "/" + fileName);
	}
	
	public static BufferedImage getImage(String fileName) throws IOException{
		return getImage(PlatFormImage.class, defaultpath + "/" + fileName);
	}
	
	public static BufferedImage getImage(String path, String fileName) throws IOException{
		return getImage(PlatFormImage.class, path + "/" + fileName);
	}
}
