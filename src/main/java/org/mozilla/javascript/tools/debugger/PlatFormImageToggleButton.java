package org.mozilla.javascript.tools.debugger;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.util.EventListener;

import javax.swing.JToggleButton;

public class PlatFormImageToggleButton extends JToggleButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	EventListener al;
	
	public PlatFormImageToggleButton(IMAGEICON img, int width, int height, EventListener al) throws Exception{
		this.al = al;
		if(al != null) {
			if(al instanceof ActionListener) addActionListener((ActionListener)al);
			else if(al instanceof MouseListener) addMouseListener((MouseListener)al);
			else throw new Exception("EventListener should be ActionListener, MouseListener or null.");
		}
		img.setSize(width, height);
//		img.setImage(img.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH));
		setMargin(new Insets(0,0,0,5));
		this.setIcon(img);
		setSize(width, height);
		
	}
	
	public PlatFormImageToggleButton(IMAGEICON img, double rate, EventListener al) throws Exception{
		this.al = al;
		if(al != null) {
			if(al instanceof ActionListener) addActionListener((ActionListener)al);
			else if(al instanceof MouseListener) addMouseListener((MouseListener)al);
			else throw new Exception("EventListener should be ActionListener, MouseListener or null.");
		}
		
		int width = (int)((double)img.getIconWidth()*rate);
		int height = (int)((double)img.getIconHeight()*rate);
		img.setSize(width, height);
//		img.setImage(img.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH));
		setMargin(new Insets(0,0,0,5));
		this.setIcon(img);
		setSize(width, height);
	}
	
	public PlatFormImageToggleButton(IMAGEICON img, EventListener al) throws Exception{
		this(img, 1, al);
	}
	
	public PlatFormImageToggleButton(IMAGEICON img, int width, int height) throws Exception{
		this(img, width, height, null);
	}
	
	public PlatFormImageToggleButton(IMAGEICON img, double rate) throws Exception{
		this(img, rate, null);
	}
	
	public PlatFormImageToggleButton(IMAGEICON img) throws Exception{
		this(img, 100, null);
	}
	
	public void setSelectedIcon(IMAGEICON img, double rate){
		int width = (int)((double)img.getIconWidth()*rate);
		int height = (int)((double)img.getIconHeight()*rate);
		img.setSize(width, height);
//		img.setImage(img.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH));
		setSelectedIcon(img);
	}
	
//	public void doClick(){
//		super.doClick();
//		if(al instanceof MouseListener) ((MouseListener)al).mouseClicked(new MouseEvent(this, 0, 0, 0, 0, 0, 1, false));;
//	}
}
