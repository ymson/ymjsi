package org.mozilla.javascript.tools.debugger;

import org.mozilla.javascript.tools.debugger.img.PlatFormImage;

import javax.swing.*;
import javax.swing.JInternalFrame.JDesktopIcon;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.List;

public class DesktopScrollPane extends JScrollPane implements InternalFrameListener {



	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JDesktopPane desktopPane;
	private InternalFrameComponentListener componentListener;
	
	JToolBar taskBar;
	ButtonGroup buttonGroup;

	ArrayList<PlatFormInternalFrame> platFormInternalFrameList;
//	int InternalFrameDefaltLocX = 0;
//	int InternalFrameDefaltLocY = 0;
//	int InternalFrameDefaltLocBaseX = 0;
//	int InternalFrameDefaltCnt = 0;
	
	/**
	 * creates the DesktopScrollPane object
	 *
	 * @param desktopPane a reference to the JDesktopPane object
	 */
	public DesktopScrollPane(JDesktopPane desktopPane, Color background) {
		componentListener = new InternalFrameComponentListener();
		buttonGroup = new ButtonGroup();
		platFormInternalFrameList = new ArrayList<PlatFormInternalFrame>();
		addComponentListener(new ComponentListener(){

			@Override
			public void componentHidden(ComponentEvent e) {}

			@Override
			public void componentMoved(ComponentEvent e) {}

			@Override
			public void componentResized(ComponentEvent e) {
				resizeDesktop();
			}

			@Override
			public void componentShown(ComponentEvent e) {}
			
		});
		this.desktopPane = desktopPane;
		this.desktopPane.setLayout(null);
		if(background != null) this.desktopPane.setBackground(background);
		desktopPane.addContainerListener(new ContainerListener() {

			public void componentAdded(ContainerEvent e) {
				onComponentAdded(e);
			}

			public void componentRemoved(ContainerEvent e) {
				onComponentRemoved(e);
			}

		});

		setViewportView(desktopPane);

		// set some defaults
		setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

	}
	
	public void setTaskBar(JToolBar taskBar){
//		taskBar.doLayout();
		this.taskBar = taskBar;
	}
	
	public void addPlatFormInternalFrame(final PlatFormInternalFrame pif) throws Exception{
		for(int i=0; i<this.desktopPane.getComponentCount(); i++){
			Object obj = this.desktopPane.getComponent(i);
			if(obj instanceof PlatFormInternalFrame){
				if(((PlatFormInternalFrame)obj).getTitle().equals(pif.getTitle())){
					throw new Exception("Plugin name has been Conflicted.");
				}
			}else{
				if(((JDesktopIcon)obj).getInternalFrame().getTitle().equals(pif.getTitle())){
					throw new Exception("Plugin name has been Conflicted.");
				}
			}
		}
		pif.setLocation(10, 10);
//		if(pif.getX() == 0 && pif.getX() == 0){
//			pif.setLocation(InternalFrameDefaltLocX, InternalFrameDefaltLocY);
//			InternalFrameDefaltCnt++;
//			InternalFrameDefaltLocX += 20;
//			InternalFrameDefaltLocY += 20;
//			if(InternalFrameDefaltCnt%10 == 0){
//				InternalFrameDefaltCnt = 0;
//				InternalFrameDefaltLocY = 0;
//				InternalFrameDefaltLocBaseX += 20;
//				InternalFrameDefaltLocX = InternalFrameDefaltLocBaseX;
//			}
//		}
		pif.addInternalFrameListener(this);
		this.desktopPane.add(pif);
		platFormInternalFrameList.add(pif);
	}
	
	public PlatFormInternalFrame getPlatFormInternalFrame(String title){
		for(int i=0; i<platFormInternalFrameList.size(); i++){
			if((platFormInternalFrameList.get(i)).getTitle().equals(title)){
				return platFormInternalFrameList.get(i);
			}
		}
		
		return null;
	}
	
	public PlatFormImageToggleButton getTeskButton(String title){
		if(taskBar == null) return null;
		for(int i=0; i<taskBar.getComponentCount(); i++){
			if(taskBar.getComponentAtIndex(i).getName().equals(title)){
				return (PlatFormImageToggleButton)taskBar.getComponentAtIndex(i);
			}
		}
		return null;
	}
	
	public void removeTaskBarButton(String title){
		if(getTeskButton(title) == null) return;
		buttonGroup.remove(getTeskButton(title));
		taskBar.remove(getTeskButton(title));
		taskBar.updateUI();
	}
	
	public PlatFormImageToggleButton addTaskBarButton(PlatFormInternalFrame pif, ActionListener al) throws Exception{
		if(taskBar == null) return null;
		TaskButton jb = new TaskButton(pif, new PlatFormImage(PlatFormImage.fileName_windowIcon));
		jb.setText(pif.getTitle());
		jb.setName(pif.getTitle());
		buttonGroup.add(jb);
		taskBar.add(jb);
		jb.setSelected(true);
		taskBar.updateUI();
		return jb;
		
	}
	
	public PlatFormImageToggleButton getTaskBarButton(String title){
		if(taskBar == null) return null;
		for(int i=0; i<taskBar.getComponentCount(); i++){
			if(taskBar.getComponentAtIndex(i).getName().equals(title)){
				return (PlatFormImageToggleButton)taskBar.getComponentAtIndex(i);
			}
		}
		return null;
	}
	
	public void autoFocus(PlatFormInternalFrame pif){
		PlatFormInternalFrame pif1 = null;
		PlatFormInternalFrame pif2 = null;
		for(int i=0; i<platFormInternalFrameList.size(); i++){
			pif2 = platFormInternalFrameList.get(i);
			
			if(!pif2.isVisible() || pif == pif2) continue;
			
			if(pif1 == null){
				pif1 = pif2;
				continue;
			}
			
			if(this.desktopPane.getComponentZOrder(pif1) > this.desktopPane.getComponentZOrder(pif2)){
				
				pif1 = pif2;
			}
		}
		
		if(pif1 != null) {
			try {
				getTaskBarButton(pif1.getTitle()).setSelected(true);
				pif1.setSelected(true);
			} catch (PropertyVetoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void internalFrameClosed(InternalFrameEvent e) {
		PlatFormInternalFrame pif = (PlatFormInternalFrame)e.getSource();
		if(pif.getDefaultCloseOperation() == JInternalFrame.DISPOSE_ON_CLOSE){
			platFormInternalFrameList.remove(pif);
		}
	}
	public void internalFrameActivated(InternalFrameEvent e) {
		PlatFormInternalFrame pif = (PlatFormInternalFrame)e.getSource();
		if(getTaskBarButton(pif.getTitle()) != null){
			getTaskBarButton(pif.getTitle()).setSelected(true);
		}
	}
	public void internalFrameClosing(InternalFrameEvent e) {
		PlatFormInternalFrame pif = (PlatFormInternalFrame)e.getSource();
		removeTaskBarButton(pif.getTitle());
		autoFocus(pif);
	}
	
	public void internalFrameDeactivated(InternalFrameEvent e) {
		try {
			PlatFormInternalFrame pif = (PlatFormInternalFrame)e.getSource();
			if(!pif.isShowing()){
				if(!pif.isIcon()) removeTaskBarButton(pif.getTitle());
				autoFocus(pif);
			}
		} catch (Exception e1) {
		}
	}
	public void internalFrameDeiconified(InternalFrameEvent e) {}
	public void internalFrameIconified(InternalFrameEvent e) {}
	public void internalFrameOpened(InternalFrameEvent e) {}
	
	private void onComponentRemoved(ContainerEvent event) {

		Component removedComponent = event.getChild();

		if (removedComponent instanceof JInternalFrame){

			removedComponent.removeComponentListener(componentListener);
			resizeDesktop();
		}
	}

	private void onComponentAdded(ContainerEvent event) {

		Component addedComponent = event.getChild();

		if (addedComponent instanceof JInternalFrame){
			addedComponent.addComponentListener(componentListener);
			resizeDesktop();
		}
	}

	/**
	 * returns all internal frames placed upon the desktop
	 *
	 * @return a JInternalFrame array containing references to the internal frames
	 */
	public JInternalFrame[] getAllFrames() {
		return desktopPane.getAllFrames();
	}

	/**
	 * sets the preferred size of the desktop
	 *
	 * @param dim a Dimension object representing the desired preferred size
	 */
	public void setDesktopSize(Dimension dim) {
		desktopPane.setPreferredSize(dim);
		desktopPane.revalidate();
	}

	/**
	 * resizes the desktop based upon the locations of its
	 * internal frames. This updates the desktop scrollbars in real-time.
	 */
	public void resizeDesktop() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run(){
				// has to go through all the internal frames now and make sure none
				// off screen, and if so, add those scroll bars!

				Rectangle viewPort = getViewport().getViewRect();

				int maxX = viewPort.width + viewPort.x, maxY = viewPort.height + viewPort.y;
				int minX = viewPort.x, minY = viewPort.y;

				// determine the min/max extents of all internal frames

				JInternalFrame frame = null;
				JInternalFrame[] frames = getAllFrames();
				
				boolean isMaximum = false;
				for (int i=0; i < frames.length; i++) {
					frame = frames[i];
					if(frame.isMaximum()) {
						isMaximum = true;
						break;
					}
				}
				
				if(!isMaximum){
					for (int i=0; i < frames.length; i++) {

						frame = frames[i];
						if(!frame.isVisible()) continue;
						
						if (frame.getX() < minX) { // get minimum X
							minX = frame.getX();
						}

						if ((frame.getX() + frame.getWidth()) > maxX){
							maxX = frame.getX() + frame.getWidth();
						}

						if (frame.getY() < minY) { // get minimum Y
							minY = frame.getY();
						}

						if ((frame.getY() + frame.getHeight()) > maxY){
							maxY = frame.getY() + frame.getHeight();
						}
					}
				}

				// Don't count with frames that get off screen from the left side ant the top

				if (minX < 0) minX = 0;
				if (minY < 0) minY = 0;

				//setVisible(false); // don't update the viewport

				// while we move everything (otherwise desktop looks 'bouncy')

				if (minX != 0 || minY != 0) {

					// have to scroll it to the right or up the amount that it's off screen...
					// before scroll, move every component to the right / down by that amount

					for (int i=0; i < frames.length; i++) {
						frame = frames[i];
						frame.setLocation(frame.getX()-minX, frame.getY()-minY);
					}

					// have to scroll (set the viewport) to the right or up the amount
					// that it's off screen...
					JViewport view = getViewport();
					view.setViewSize(new Dimension((maxX-minX),(maxY-minY)));
					view.setViewPosition(new Point((viewPort.x-minX),(viewPort.y-minY)));
					setViewport(view);
				}

				// resize the desktop
				setDesktopSize(new Dimension(maxX-minX, maxY-minY));
				//setVisible(true); // update the viewport again
			}
		});
	}
	
	public void setSelectedFrame(JInternalFrame f){
		desktopPane.setSelectedFrame(f);
	}

	private class InternalFrameComponentListener implements ComponentListener{

		public void componentResized(ComponentEvent e) {
			resizeDesktop();
		}

		public void componentMoved(ComponentEvent e) {
			resizeDesktop();
		}

		public void componentShown(ComponentEvent e) {
			
			try {
				final PlatFormInternalFrame pif = (PlatFormInternalFrame)e.getSource();
				addTaskBarButton(pif, null);
				resizeDesktop();
				pif.requestFocusInWindow();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		public void componentHidden(final ComponentEvent e) {
			//PlatFormInternalFrame pif = (PlatFormInternalFrame)e.getSource();
			//MainFrame.getMainFrame().removeTaskBarButton(pif.getTitle());
			resizeDesktop();
		}

	}
	
	public void setTile(){
		getVerticalScrollBar().setValue(0);
		getHorizontalScrollBar().setValue(0);
		JInternalFrame[] allFrames = this.desktopPane.getAllFrames();
		List<JInternalFrame> realFrames = new ArrayList<JInternalFrame>();
		for(JInternalFrame f : allFrames){
			try {
                f.setIcon(false);
                f.setMaximum(false);
            } catch (Exception exc) {
            }
			if(f.isShowing()){
				realFrames.add(f);
			}
		}
        int count = realFrames.size();
        if(count <= 0) return;
        int rows, cols;
        rows = cols = (int)Math.sqrt(count);
        if (rows*cols < count) {
            cols++;
            if (rows * cols < count) {
                rows++;
            }
        }
        Dimension size = this.getViewport().getSize();
        int verticalScrollbarSize = (int) ((this.getVerticalScrollBar().isVisible()) ? this.getVerticalScrollBar().getSize().getWidth(): 0);
        int horizontalScrollbarSize = (int) ((this.getHorizontalScrollBar().isVisible()) ? this.getHorizontalScrollBar().getSize().getHeight(): 0);
        int w = (size.width + verticalScrollbarSize)/cols;
        int h = (size.height + horizontalScrollbarSize)/rows;
        int x = 0;
        int y = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                int index = (i*cols) + j;
                if (index >= realFrames.size()) {
                    break;
                }
                JInternalFrame f = realFrames.get(index);
                try {
                    f.setIcon(false);
                    f.setMaximum(false);
                } catch (Exception exc) {
                }
                this.desktopPane.getDesktopManager().setBoundsForFrame(f, x, y,
                                                           w, h);
                x += w;
            }
            y += h;
            x = 0;
        }
	}
	
	public void setCascade(){
		getVerticalScrollBar().setValue(0);
		getHorizontalScrollBar().setValue(0);
		JInternalFrame[] allFrames = this.desktopPane.getAllFrames();
		List<JInternalFrame> realFrames = new ArrayList<JInternalFrame>();
		for(JInternalFrame f : allFrames){
			try {
                f.setIcon(false);
                f.setMaximum(false);
            } catch (Exception exc) {
            }
			if(f.isShowing()){
				realFrames.add(f);
			}
		}
		int count = realFrames.size();
		if(count <= 0) return;
        int x, y, w, h;
        x = y = 0;
        h = (int) this.getViewport().getSize().getHeight();
        int d = h / count;
        if (d > 30) d = 30;
        for (int i = count -1; i >= 0; i--, x += d, y += d) {
            JInternalFrame f = realFrames.get(i);
            try {
                f.setIcon(false);
                f.setMaximum(false);
            } catch (Exception exc) {
            }
            Dimension dimen = f.getSize();
            w = dimen.width;
            h = dimen.height;
            this.desktopPane.getDesktopManager().setBoundsForFrame(f, x, y, w, h);
        }
	}
	
	class TaskButton extends PlatFormImageToggleButton{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public TaskButton(final PlatFormInternalFrame pif, IMAGEICON img) throws Exception{
			super(img, 15, 15);
			addActionListener(new ActionListener(){

				@Override
				public void actionPerformed(ActionEvent e) {
					SwingUtilities.invokeLater(new Runnable(){

						@Override
						public void run() {
							try {
								if(pif.isIcon()){
									pif.setIcon(false);
									pif.setSelected(true);
								}else{
									if(!pif.isSelected()){
										pif.setSelected(true);
									}else{
										pif.setIcon(true);
										pif.setSelected(false);
										TaskButton.this.setSelected(false);
									}
								}
							} catch (PropertyVetoException e1) {
							}
						}
						
					});
				}
				
			});
		}
		
	}

}