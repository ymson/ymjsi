package org.mozilla.javascript;


public class AssertException extends EvaluatorException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AssertException(String detail)
    {
        super(detail);
    }
	
	public AssertException(boolean state, String detail, String sourceName,
            int lineNumber)
	{
		this(state, detail, sourceName, lineNumber, null, 0);
	}
	
	public AssertException(boolean state, String detail, String sourceName, int lineNumber,
            String lineSource, int columnNumber)
	{
		super(detail);
		recordErrorOrigin(sourceName, lineNumber, lineSource, columnNumber);
	}
	
	/**
     * @deprecated Use {@link RhinoException#sourceName()} from the super class.
     */
    public String getSourceName()
    {
        return sourceName();
    }

    /**
     * @deprecated Use {@link RhinoException#lineNumber()} from the super class.
     */
    public int getLineNumber()
    {
        return lineNumber();
    }

    /**
     * @deprecated Use {@link RhinoException#columnNumber()} from the super class.
     */
    public int getColumnNumber()
    {
        return columnNumber();
    }

    /**
     * @deprecated Use {@link RhinoException#lineSource()} from the super class.
     */
    public String getLineSource()
    {
        return lineSource();
    }
}
